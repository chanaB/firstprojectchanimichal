﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventExample
{
    class Program
    {
        //b. create the event function
      private static void ShowFuelMassage(object sender,FuelEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("to "+(sender as Car).Driver+" fuel left: "+e.Fuel);
            Console.ResetColor();
            
        }
        static void Main(string[] args)
        {
            //a. create an object from the class
            //Car c = new Car("Dan");
            //c. send the function address to the event
            // c.NotEnoughFuel += ShowFuelMassage;
            // c.Drive();
            Form frm = new Form();
            frm.MouseMove += Frm_MouseMove;
            frm.ShowDialog();
        }

        private static void Frm_MouseMove(object sender, MouseEventArgs e)
        {
            (sender as Form).Text = e.X + " " + e.Y;
            Console.WriteLine(e.X+" "+e.Y+"aaaaaaaa");
            Console.WriteLine("Michal");
        }
    }
}
