﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventExample
{//1.create a delegate type
    delegate void fuelHandler(object sender,FuelEventArgs e);
    class Car
    {
        public string Driver;
        //2. create an event variable
        public event fuelHandler NotEnoughFuel;
        public Car(string driver)
        {
            Driver = driver;
        }
        public void Drive()
        {
            int fuel = 10;
            for (int i =1; i <=10; i++)
            {
                Thread.Sleep(1000);
                fuel--;
                Console.WriteLine("driving......");
                //3.call the event function
                if(fuel<=3)
                {
                    if(NotEnoughFuel!=null)
                       NotEnoughFuel(this,new FuelEventArgs(fuel));
                }
            }
            Console.WriteLine("end driving.");
        }
    }
}
